import { store } from 'react-easy-state'
import moment from 'moment'
export default store({
  transactions: null,
  filter(from, to) {
    return _.debounce(() => {
      this.load(from, to)
    }, 1000)
  },
  async load(from, to) {
    if (!this.running) {
      this.running = true
      const f = from || moment(0).unix()
      const t = to || moment().unix()
      const query = `
    {
      transactions(from: ${f}, to: ${t}) {
        total
        currency
        received
        type
        hash
        converted {
          total
          fees
        }
      }
    }
    `
      this.transactions = await axios.post('/graphql', { query }).then(d => d.data.data.transactions)
      this.running = false
    }
  }
})
