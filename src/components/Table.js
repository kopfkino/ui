import { view } from 'react-easy-state'
import ReactTable from 'react-table'
import moment from 'moment'
export default view(class extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render() {
    return (
      <div className='card table'>
        <div className='card-body flex-column'>
          <ReactTable
            data={this.props.store.transactions}
            className='transaction-table'
            style={{ height: '100%' }}
            columns={[
              {
                Header: 'Hash',
                accessor: 'hash'
              },
              {
                Header: 'Amount',
                id: 'total',
                accessor: d => `${d.total.toFixed(4)} ${d.currency}`
              },
              {
                Header: 'ETH',
                id: 'eth',
                accessor: d => d.converted.total.toFixed(4)
              },
              {
                Header: 'Received',
                id: 'received',
                accessor: d => `${moment.unix(d.received).format('DD.MM HH:mm')}`
              },
              {
                Header: 'Type',
                accessor: 'type'
              }
            ]}
          />
        </div>
      </div>
    )
  }
})
