import { view } from 'react-easy-state'
import { ResponsiveContainer, PieChart, Pie, Tooltip, Legend } from 'recharts'
export default view(class extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
    this.load = () => {
      const grouped = _.groupBy(this.props.store.transactions, tx => tx.type)
      const data = _.keys(grouped).map(c => {
        return {
          currency: c,
          total: _.sumBy(grouped[c], t => t.converted.total)
        }
      })
      this.setState({ data })
    }
  }
  componentDidMount() {
    this.load()
  }
  componentWillReceiveProps() {
    this.load()
  }
  render() {
    return (
      <div className='card side-chart flex-column'>
        <div className='card-body fill flex-column flex-center'>
          <ResponsiveContainer width='90%' aspect={1.0} minWidth={140}>
            <PieChart>
              <Tooltip formatter={value => `${value.toFixed(3)} ETH`} />
              <Pie
                data={this.state.data}
                margin={{ top: 5, right: 5, bottom: 5, left: 5 }}
                dataKey='total'
                nameKey='currency'
                labelLine={false}
                outerRadius={100}
                label={l => {
                  const c = (
                    <text fill="#061258" x={l.x} y={l.y} textAnchor='middle' dominantBaseline="central">
                      {l.name.toUpperCase()}
                    </text>
                  )
                  return c
                }}
                fill='#3de3ae'
              />
            </PieChart>
          </ResponsiveContainer>
        </div>
      </div>
    )
  }
})
