import { view } from 'react-easy-state'
import moment from 'moment'
import { ResponsiveContainer, BarChart, Bar, Tooltip, XAxis, YAxis, Brush } from 'recharts'
const round = (date, duration, method) => {
  return moment(Math[method]((+date) / (+duration)) * (+duration))
}
export default view(class extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
    this.setLimits = (e) => {
      const from = moment(this.state.data[e.startIndex].day).unix()
      const to = moment(this.state.data[e.endIndex].day).unix()
      this.props.store.filter(from, to)()
    }
  }
  componentDidMount() {
    const days = _.groupBy(_.sortBy(this.props.store.transactions, tx => tx.received), tx => round(moment.unix(tx.received), moment.duration(3, 'hours'), 'ceil'))
    const data = _.keys(days).map(day => {
      return {
        day,
        total: _.sumBy(days[day], w => w.converted.total).toFixed(4)
      }
    })
    this.setState({ data })
  }

  render() {
    return (
      <div className='card top-chart flex-column'>
        <div className='card-body fill'>
          <ResponsiveContainer width='100%' height={105} className='flex-column'>
            <BarChart data={this.state.data}>
              <Tooltip formatter={value => `${value} ETH`} labelFormatter={label => moment(label).format('DD.MM HH:mm')} />
              <XAxis hide dataKey='day' />
              <YAxis type='number' allowDataOverflow={true} hide domain={[0, 1000]} />
              <Bar dataKey='total' style={{ marginBottom: '10px' }} />
              <Brush y={82} dateKey='day' stroke='#3866FF' height={23} onChange={this.setLimits} />
            </BarChart>

          </ResponsiveContainer>
        </div>
      </div>
    )
  }
})
