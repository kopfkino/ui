import { hot } from 'react-hot-loader'
import { view } from 'react-easy-state'
import TopChart from './TopChart'
import Table from './Table'
import SideChart1 from './SideChart1'
import SideChart2 from './SideChart2'
import SideChart3 from './SideChart3'
class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render() {
    const store = this.props.store
    return (
      <div className='fill flex-column'>
        {
          !store.transactions && (
            <div className='fill flex-column flex-center'>
              <div className='loading loading-lg' />
            </div>
          )
        }
        {
          store.transactions && (
            <div className='fill flex-column'>
              <TopChart {...this.props} />
              <div className='fill flex-row margin-top'>
                <Table {...this.props} />
                <div className='flex-column'>
                  <SideChart1 {...this.props} />
                  <SideChart2 {...this.props} />
                  <SideChart3 {...this.props} />
                </div>
              </div>
            </div>
          )
        }
      </div>
    )
  }
}
export default hot(module)(view(App))
