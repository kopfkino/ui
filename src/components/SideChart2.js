import { view } from 'react-easy-state'
export default view(class extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }
  render() {
    const store = this.props.store
    return (
      <div className='card side-chart flex-center'>
        <div className='card-body'>
          <div className='stat flex-column flex-center'>
            <span className='big'>
              {_.sumBy(store.transactions, t => t.converted.total).toFixed(2)}
            </span>
            <span className='sub'>ETH</span>
          </div>
        </div>
      </div>
    )
  }
})
