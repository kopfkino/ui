FROM node:alpine as build
COPY . /app
WORKDIR /app
RUN npm install --only production
RUN npm run build

FROM abiosoft/caddy
WORKDIR /app
RUN mkdir -p /srv
COPY --from=build /app/dist /srv
COPY Caddyfile /etc/Caddyfile